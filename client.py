
# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.


"""
An example client. Run simpleserv.py first before running this.
"""
from __future__ import print_function

from twisted.internet import reactor, protocol
from twisted.internet.endpoints import TCP4ClientEndpoint
from playground.twisted.endpoints import GateClientEndpoint, GateServerEndpoint

import sys



# a client protocol

class EchoClient(protocol.Protocol):
    """Once connected, send a message, then print the result."""
    
   
    def connectionMade(self):
	#line1 = "GET /test/test.html HTTP/1.0"
        #self.transport.write(sys.argv[1])
        self.transport.write("GET " + sys.argv[1] + " HTTP/1.1")
    
    def dataReceived(self, data):
        "As soon as any data is received, write it back."
        print("Server said:", data)
        self.transport.loseConnection()
    
    def connectionLost(self, reason):
        print("connection lost")

class EchoFactory(protocol.ClientFactory):
    protocol = EchoClient

    def clientConnectionFailed(self, connector, reason):
        print("Connection failed - goodbye!")
        reactor.stop()
    
    def clientConnectionLost(self, connector, reason):
        print("Connection lost - goodbye!")
        reactor.stop()


# this connects the protocol to a server running on port 8000
def main():
    endpoint = GateClientEndpoint.CreateFromConfig(reactor, "20164.0.0.1", 101, "gatekey2")
    #point = TCP4ClientEndpoint(reactor, "localhost", 8000)
    #d = point.connect(EchoFactory())
    endpoint.connect(EchoFactory())
    reactor.run()

# this only runs if the module was *not* imported
if __name__ == '__main__':
    main()
