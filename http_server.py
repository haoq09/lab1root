
# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.


from twisted.internet import reactor, protocol
from twisted.internet.endpoints import TCP4ServerEndpoint
from playground.twisted.endpoints import GateServerEndpoint, GateClientEndpoint


class Echo(protocol.Protocol):
    """This is just about the simplest possible protocol"""
    
    def dataReceived(self, data):
        "As soon as any data is received, write it back."
	print(data)
	status = data.split(' ', 3)
 	first = status[0]
	end = status[2]
	if first != "GET" or end != "HTTP/1.1":
		self.transport.write('\n404 File Not Found\n')
			
	else:
		filename = "./"
		if (".html" in status[1]):
			filename = filename + status[1]
		else:
			#When user only sends a directory name like "test"
			filename = filename + status[1] + "/index.html"
		getPage(self,filename)
		

def getPage(self, filename):
	try:

		f = open(filename)
       		output = f.read()
		f.close();
		contentLength = str(len(output))
		self.transport.write('HTTP/1.0 200 OK\r\nContent-Length:' + contentLength 			+ '\r\nContent-Type: text/html\r\n\r\n')
		self.transport.write(output)

	except IOError:
		self.transport.write('\n404 File Not Found\n')

class HttpFactory(protocol.ServerFactory):
	protocol = Echo

def main():
    """This runs the protocol on port 8000"""
    endpoint = GateServerEndpoint.CreateFromConfig(reactor, 101, "gatekey1")
    #endpoint = TCP4ServerEndpoint(reactor, 8000)
    endpoint.listen(HttpFactory())
    reactor.run()

# this only runs if the module was *not* imported
if __name__ == '__main__':
    main()
